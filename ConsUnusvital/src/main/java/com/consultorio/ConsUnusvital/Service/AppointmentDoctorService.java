package com.consultorio.ConsUnusvital.Service;

import com.consultorio.ConsUnusvital.Models.AppointmentDoctor;
import java.util.List;
public interface AppointmentDoctorService {
    
    public AppointmentDoctor save(AppointmentDoctor appointmentdoctor);
    public void delete (String id);
    public AppointmentDoctor findByid(String id);
    public List<AppointmentDoctor> findByall();
    
}
