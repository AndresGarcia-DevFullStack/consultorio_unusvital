
package com.consultorio.ConsUnusvital.Service;

import com.consultorio.ConsUnusvital.Models.Formula;
import java.util.List;
public interface FormulaService {
     public Formula save(Formula formula);
     public void delete (String id);
     public  Formula findByid(String id);
     public List<Formula> findByall(); 
    
    
}
