
package com.consultorio.ConsUnusvital.Service;

import com.consultorio.ConsUnusvital.Models.Appointment;
import java.util.List;
public interface AppointmentService {
    
    public Appointment save(Appointment appointment);
    public void delete (String id);
    public Appointment findByid(String id);
    public List<Appointment> findByall();
    
}
