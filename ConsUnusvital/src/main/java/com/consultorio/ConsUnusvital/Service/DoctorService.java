/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consultorio.ConsUnusvital.Service;

import com.consultorio.ConsUnusvital.Models.Doctor;
import java.util.List;

public interface DoctorService {
     public Doctor save(Doctor doctor);
     public void delete (String id);
     public  Doctor findByid(String id);
     public List<Doctor> findByall(); 
    
}
