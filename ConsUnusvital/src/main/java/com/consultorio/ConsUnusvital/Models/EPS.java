
package com.consultorio.ConsUnusvital.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Table
@Entity (name="EPS")
public class EPS {
    @Id
    @Column (name="EPSCode")
    private String EPSCode;
    @Column
    private String NameEPS;

    public EPS(String EPSCode, String NameEPS) {
        this.EPSCode = EPSCode;
        this.NameEPS = NameEPS;
    }

    public EPS() {
    }

    public String getEPSCode() {
        return EPSCode;
    }

    public void setEPSCode(String EPSCode) {
        this.EPSCode = EPSCode;
    }

    public String getNameEPS() {
        return NameEPS;
    }

    public void setNameEPS(String NameEPS) {
        this.NameEPS = NameEPS;
    }
    
    
    
}
