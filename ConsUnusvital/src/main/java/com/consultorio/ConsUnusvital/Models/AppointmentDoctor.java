
package com.consultorio.ConsUnusvital.Models;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Table
@Entity (name="appointmentDoctor")
public class AppointmentDoctor {
    @Id
    @Column (name="DoctorCode")
    private String DoctorCode;
    @Id
    @Column (name="AppointmentCode")
    private String AppointmentCode;

    public AppointmentDoctor(String DoctorCode, String AppointmentCode) {
        this.DoctorCode = DoctorCode;
        this.AppointmentCode = AppointmentCode;
    }

    public AppointmentDoctor() {
    }

    public String getDoctorCode() {
        return DoctorCode;
    }

    public void setDoctorCode(String DoctorCode) {
        this.DoctorCode = DoctorCode;
    }

    public String getAppointmentCode() {
        return AppointmentCode;
    }

    public void setAppointmentCode(String AppointmentCode) {
        this.AppointmentCode = AppointmentCode;
    }
    
    
    
    
}
