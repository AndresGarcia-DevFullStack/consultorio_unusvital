
package com.consultorio.ConsUnusvital.DAO;

import com.consultorio.ConsUnusvital.Models.Drug;
import org.springframework.data.repository.CrudRepository;
public interface DrugDAO extends CrudRepository<Drug, String>{
    
}
