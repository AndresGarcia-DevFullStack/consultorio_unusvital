
package com.consultorio.ConsUnusvital.DAO;

import com.consultorio.ConsUnusvital.Models.FormulaDrug;
import org.springframework.data.repository.CrudRepository;

public interface FormulaDrugDAO extends CrudRepository <FormulaDrug, String> {
    
}
