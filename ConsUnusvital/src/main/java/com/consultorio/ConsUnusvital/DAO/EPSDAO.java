
package com.consultorio.ConsUnusvital.DAO;

import com.consultorio.ConsUnusvital.Models.EPS;
import org.springframework.data.repository.CrudRepository;

public interface EPSDAO extends CrudRepository<EPS, String>{
    
}
