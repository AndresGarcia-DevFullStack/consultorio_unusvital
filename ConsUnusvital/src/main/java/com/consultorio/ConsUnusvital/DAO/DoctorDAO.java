
package com.consultorio.ConsUnusvital.DAO;
import com.consultorio.ConsUnusvital.Models.Doctor;
import org.springframework.data.repository.CrudRepository;
public interface DoctorDAO extends CrudRepository<Doctor, String>{
    
}
