package com.consultorio.ConsUnusvital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsUnusvitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsUnusvitalApplication.class, args);
	}

}
