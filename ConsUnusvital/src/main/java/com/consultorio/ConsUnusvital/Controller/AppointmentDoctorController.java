
package com.consultorio.ConsUnusvital.Controller;

import com.consultorio.ConsUnusvital.Models.AppointmentDoctor;
import com.consultorio.ConsUnusvital.Service.AppointmentDoctorService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/AppointmentDoctor")
public class AppointmentDoctorController {
    @Autowired
    private AppointmentDoctorService AppointmentDoctorService;
    
    @PostMapping(value="/")
    public ResponseEntity <AppointmentDoctor> agregar(@RequestBody AppointmentDoctor appointmentdoctor){
        AppointmentDoctor obj=AppointmentDoctorService.save(appointmentdoctor);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @DeleteMapping(value="/{id}")
    public ResponseEntity<AppointmentDoctor> eliminar(@PathVariable String id){
        AppointmentDoctor obj = AppointmentDoctorService.findByid(id);
        if (obj!=null)
            AppointmentDoctorService.delete(id);
        else
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @PutMapping(value="/")
    public ResponseEntity<AppointmentDoctor> editar(@RequestBody AppointmentDoctor appointmentdoctor){
        AppointmentDoctor obj = AppointmentDoctorService.findByid(appointmentdoctor.getDoctorCode());
        if(obj!=null){
            obj.setDoctorCode(appointmentdoctor.getDoctorCode());
           
            AppointmentDoctorService.save(obj);
            
            
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @GetMapping("/List")
    public List<AppointmentDoctor> consultarTodo(){
        return AppointmentDoctorService.findByall();
    }
    @GetMapping("/List/{id}")
    public AppointmentDoctor consultaPorId(@PathVariable String id){
        return AppointmentDoctorService.findByid(id);
    }
 
}
