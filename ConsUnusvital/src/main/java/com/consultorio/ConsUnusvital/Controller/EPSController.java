package com.consultorio.ConsUnusvital.Controller;

import com.consultorio.ConsUnusvital.Models.EPS;
import com.consultorio.ConsUnusvital.Service.EPSService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/EPS")
public class EPSController {
    @Autowired
    private EPSService EPSService;
    
    @PostMapping(value="/")
    public ResponseEntity <EPS> agregar(@RequestBody EPS eps){
        EPS obj=EPSService.save(eps);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @DeleteMapping(value="/{id}")
    public ResponseEntity<EPS> eliminar(@PathVariable String id){
        EPS obj = EPSService.findByid(id);
        if (obj!=null)
            EPSService.delete(id);
        else
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
   
    @PutMapping(value="/")
    public ResponseEntity<EPS> editar(@RequestBody EPS eps){
        EPS obj = EPSService.findByid(eps.getNameEPS());
        if(obj!=null){
            obj.setNameEPS(eps.getNameEPS());
           
            EPSService.save(obj);
            
            
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @GetMapping("/List")
    public List<EPS> consultarTodo(){
        return EPSService.findByall();
    }
    @GetMapping("/List/{id}")
    public EPS consultaPorId(@PathVariable String id){
        return EPSService.findByid(id);
    }
 
}
