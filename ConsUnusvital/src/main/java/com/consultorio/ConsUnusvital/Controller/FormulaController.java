package com.consultorio.ConsUnusvital.Controller;

import com.consultorio.ConsUnusvital.Models.Formula;
import com.consultorio.ConsUnusvital.Service.FormulaService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/Formula")
public class FormulaController {
    @Autowired
    private FormulaService FormulaService;
    
    @PostMapping(value="/")
    public ResponseEntity <Formula> agregar(@RequestBody Formula formula){
        Formula obj=FormulaService.save(formula);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Formula> eliminar(@PathVariable String id){
        Formula obj = FormulaService.findByid(id);
        if (obj!=null)
            FormulaService.delete(id);
        else
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
   
    @PutMapping(value="/")
    public ResponseEntity<Formula> editar(@RequestBody Formula formula){
        Formula obj = FormulaService.findByid(formula.getConsecutivo_formula());
        if(obj!=null){
            obj.setConsecutivo_formula(formula.getConsecutivo_formula());
           
            FormulaService.save(obj);
            
            
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @GetMapping("/List")
    public List<Formula> consultarTodo(){
        return FormulaService.findByall();
    }
    @GetMapping("/List/{id}")
    public Formula consultaPorId(@PathVariable String id){
        return FormulaService.findByid(id);
    }
 
}
