
package com.consultorio.ConsUnusvital.Controller;

import com.consultorio.ConsUnusvital.Models.Registropaciente;
import com.consultorio.ConsUnusvital.Service.RegistropacienteService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/Registropaciente")
public class RegistropacienteController {
    @Autowired
    private RegistropacienteService RegistropacienteService;
    
    @PostMapping(value="/")
    public ResponseEntity <Registropaciente> agregar(@RequestBody Registropaciente registropaciente){
        Registropaciente obj=RegistropacienteService.save(registropaciente);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Registropaciente> eliminar(@PathVariable String id){
        Registropaciente obj = RegistropacienteService.findByid(id);
        if (obj!=null)
            RegistropacienteService.delete(id);
        else
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @PutMapping(value="/")
    public ResponseEntity<Registropaciente> editar(@RequestBody Registropaciente registropaciente){
        Registropaciente obj = RegistropacienteService.findByid(registropaciente.getId_paciente());
        if(obj!=null){
            obj.setNombre_pac(registropaciente.getNombre_pac());
            obj.setE_mail(registropaciente.getE_mail());
            obj.setTelefono_pac(registropaciente.getTelefono_pac());
            RegistropacienteService.save(obj);
            
            
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @GetMapping("/List")
    public List<Registropaciente> consultarTodo(){
        return RegistropacienteService.findByall();
    }
    @GetMapping("/List/{id}")
    public Registropaciente consultaPorId(@PathVariable String id){
        return RegistropacienteService.findByid(id);
    }
 
}
